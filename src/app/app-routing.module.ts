import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { ConnectedGuard } from './guards/connected.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    canActivate: [ConnectedGuard],
    loadChildren: './login/login.module#LoginPageModule'
  },
  {
    path: 'home',
    canActivate: [AuthGuard, ConnectedGuard],
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'data',
    canActivate: [AuthGuard, ConnectedGuard],
    loadChildren: () => import('./data/data.module').then(m => m.DataPageModule)
  },
  {
    path: 'time-out',
    loadChildren: () => import('./time-out/time-out.module').then(m => m.TimeOutPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
