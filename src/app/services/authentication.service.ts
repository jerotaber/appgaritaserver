import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { catchError, timeout } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private urlAPI = 'https://mapp.com.ar/api/garita/login';
  authenticationState = new BehaviorSubject(false);
  constructor(private storage: Storage, private plt: Platform, private httpClient: HttpClient) {

  }

  logout() {
    localStorage.removeItem("token");
    this.authenticationState.next(false);

  }


  autenticate(user, password) {
    var form = new FormData();
    form.append("user", user);
    form.append("password", password);
    return this.httpClient.post(this.urlAPI, form).pipe(
      timeout(10000),
      catchError(err => {
        console.log('Error');
        return throwError(err);
      })
    );
  }

  setSession(authResult) {
    localStorage.setItem('token', authResult.token);
    this.authenticationState.next(true);
    console.log(localStorage.getItem('token'));
  }
  public isLoggedIn() {
    return this.authenticationState.value;
  }

  
}
