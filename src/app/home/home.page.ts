import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  private token: string = localStorage.getItem('token');
  constructor(private authService: AuthenticationService, private router: Router, private network: Network) { }

  ngOnInit() {
    this.network.onDisconnect().subscribe(() => {
      this.router.navigateByUrl('/time-out');
    });
    var networkState = this.network.type;
    console.log(networkState);

    if (networkState == 'none') {
      this.router.navigateByUrl('/time-out');
    }
  }

  logout() {
    this.authService.logout();
  }
  ingressPage() {
    this.router.navigateByUrl('/data');
  }
  
}