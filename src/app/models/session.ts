import { Observable } from "rxjs";
export class Session {
    public token: string;
    public status: boolean;
}