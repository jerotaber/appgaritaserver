import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Network } from '@ionic-native/network/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  constructor(
    public loadingCtrl: LoadingController,
    private authService: AuthenticationService,
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private network: Network,
    private router: Router
  ) {

    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]]
    });
  }

  ngOnInit() {
    this.network.onDisconnect().subscribe(() => {
      this.router.navigateByUrl('/time-out');
    });
    var networkState = this.network.type;
    console.log(networkState);

    if (networkState == 'none') {
      this.router.navigateByUrl('/time-out');
    }
  }

  async verificarUsuario() {
    const loading = await this.loadingCtrl.create({
      message: 'Verificando'
    });
    loading.present();

    const user = this.loginForm.get(['username']).value;
    const password = this.loginForm.get(['password']).value;
    this.authService.autenticate(user, password).subscribe(responseData => {
      console.log(responseData);
      setTimeout(() => {
        loading.dismiss();
        this.authService.setSession(responseData)
      }, 1000);
    },
      (error) => {
        console.log(error);
        setTimeout(() => {
          loading.dismiss();
          this.presentAlertFailed(error.error);
        }, 1000);
      });
  }

  async presentAlertFailed(message) {
    const alert = await this.alertCtrl.create({
      subHeader: 'Hubo un problema al iniciar sesion',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}