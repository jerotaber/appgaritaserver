import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IngressService } from '../services/ingress.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { Marcar } from '../models/marcar';
import { Network } from '@ionic-native/network/ngx';
import { Router } from '@angular/router';
@Component({
  selector: 'app-data',
  templateUrl: './data.page.html',
  styleUrls: ['./data.page.scss'],
})

export class DataPage implements OnInit {
  public ingressForm: FormGroup;
  private log: Marcar[];

  constructor(
    public formBuilder: FormBuilder, 
    public ingressService: IngressService, 
    private alertCtrl: AlertController, 
    public loadingCtrl: LoadingController,
    private network: Network,
    private router: Router
    ) {
    this.ingressForm = this.formBuilder.group({
      dniString: ['', [Validators.required, Validators.maxLength(50)]]
    });

  }

  ngOnInit() {
    this.network.onDisconnect().subscribe(() => {
      this.router.navigateByUrl('/time-out');
    });
    var networkState = this.network.type;
    console.log(networkState);

    if (networkState == 'none') {
      this.router.navigateByUrl('/time-out');
    }
    this.log = JSON.parse(localStorage.getItem('log'));
  }

  /////////////SEND POST TO SERVER//////////////
  async sendIngress() {
    const loading = await this.loadingCtrl.create({
      message: 'Verificando'
    });
    loading.present();
    const dniString = this.ingressForm.get(['dniString']).value;
    this.ingressService.sendPost(dniString).
      subscribe(responseData => {
        console.log({ ...responseData });
        setTimeout(() => {
          loading.dismiss();
          this.pushArray(responseData);
          this.presentAlertSucces(responseData);
        }, 1000);
      },
        (error) => {
          setTimeout(() => {
            loading.dismiss();
            this.pushArray(error.error);
            this.presentAlertFailed(error.error);
          }, 1000);
        });
  }

  /////////PUSH NEW ITEM IN ARRAY////////////
  pushArray(responseData) {
    console.log({ ...this.log });
    console.log(this.log);

    this.log = JSON.parse(localStorage.getItem('log'));
    if (this.log != null && this.log.length > 9) {
      this.log.splice(0, 1);
    }
    if (this.log == null) {
      this.log = [];
    }
    this.log.push(responseData);
    localStorage.setItem('log', JSON.stringify(this.log));
    console.log({ ...this.log });
    console.log(this.log);


  }
  //////////////////////////////////////////
  ////////////ALERT MESSAGES///////////////
  async presentAlertFailed(message) {
    const alert = await this.alertCtrl.create({
      subHeader: 'Hubo un problema al ingresar al usuario',
      message: JSON.stringify(message),
      buttons: ['OK']
    });
    await alert.present();
  }
  async presentAlertSucces(message) {
    console.log(message);
    const alert = await this.alertCtrl.create({
      subHeader: 'El usuario se ingreso con exito',
      message: JSON.stringify(message),
      buttons: ['OK']
    });
    await alert.present();
  }
  //////////////////////////////////////////
}
