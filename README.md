App Garita Server
## Instalaciones Necesarias

Para correr correctamente la app, es necesario la instalacion de las siguientes herramientas:

1. Apache Cordova [Link](https://cordova.apache.org/#getstarted).
2. JDK y Android SDK [Link](https://cordova.apache.org/docs/es/latest/guide/platforms/android/).
3. Node JS [Link](https://nodejs.org/es/).
4. Ionic cli [Link](https://ionicframework.com/docs/installation/cli).
5. Cordova [Link](https://ionicframework.com/docs/v3/intro/installation/).

---

## Creacion de un nuevo proyecto ionic-cordova

Para crear un nuevo proyecto ionic-cordova en android, se deben correer los siguientes comandos en la terminal:
---
```
ionic start [name] [type]
```
---
```
ionic cordova prepare android
```
---
Para crear la apk de este proyecto se utiliza el comando:
---
```
ionic cordova build android
```
---

## Plugins necesarios para este proyecto
-Phonegap-nfc [Link](https://github.com/apache/cordova-plugin-network-information).
[Guia instalacion ionic](https://ionicframework.com/docs/native/network).

Instalacion
Network
```
ionic cordova plugin add cordova-plugin-network-information
npm install @ionic-native/network
```

## Caso de uso
una vez descargado el proyecto se puede correr en el browser con el codigo:
```
ionic serve
```
O crear una apk con:
```
ionic cordova build android
```
---
La apk se enciuentra en la carpeta:

../platforms/android/app/build/outputs/apk/debug

El archivo por default es:
app-debug.apk

Dicho archivo debe ser transferido al mobile e instalado
Una vez instalado, simplemente debe abrirse, automaticamente reconocera tarjetas nfc si son colocadas en el lector correspondiente.



